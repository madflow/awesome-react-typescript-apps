# Awesome React and Typescript apps

> An awesome list of real-world open source app repositories - using React and Typescript. No demos and xyz clones.



- [grafana](https://github.com/grafana/grafana) - The open and composable observability and data visualization platform. Visualize metrics, logs, and traces from multiple sources like Prometheus, Loki, Elasticsearch, InfluxDB, Postgres and many more.  - 51766  ⭐ - GNU Affero General Public License v3.0
  

- [sentry](https://github.com/getsentry/sentry) - Developer-first error tracking and performance monitoring - 32394  ⭐ - Other
  

- [appsmith](https://github.com/appsmithorg/appsmith) - Low code project to build admin panels, internal tools, and dashboards. Integrates with 15&#43; databases and any API. - 22297  ⭐ - Apache License 2.0
  

- [kibana](https://github.com/elastic/kibana) - Your window into the Elastic Stack - 17959  ⭐ - Other
  

- [wp-calypso](https://github.com/Automattic/wp-calypso) - The JavaScript and API powered WordPress.com - 12134  ⭐ - GNU General Public License v2.0
  

- [posthog](https://github.com/PostHog/posthog) - 🦔 PostHog provides open-source product analytics, session recording, feature flagging and a/b testing that you can self-host.  - 9680  ⭐ - Other
  

- [webamp](https://github.com/captbaritone/webamp) - Winamp 2 reimplemented for the browser - 8818  ⭐ - MIT License
  

- [devhub](https://github.com/devhubapp/devhub) - TweetDeck for GitHub - Filter Issues, Activities &amp; Notifications - Web, Mobile &amp; Desktop with 99% code sharing between them - 8518  ⭐ - GNU Affero General Public License v3.0
  

- [tinacms](https://github.com/tinacms/tinacms) - A headless CMS for Markdown - 8011  ⭐ - Apache License 2.0
  

- [hospitalrun-frontend](https://github.com/HospitalRun/hospitalrun-frontend) - Frontend for HospitalRun - 6659  ⭐ - MIT License
  

- [openreplay](https://github.com/openreplay/openreplay) - :tv: OpenReplay is developer-friendly, open-source session replay. - 6116  ⭐ - Other
  

- [learnapollo](https://github.com/learnapollo/learnapollo) - 👩🏻‍🏫   Learn Apollo - A hands-on tutorial for Apollo GraphQL Client (created by Graphcool) - 5248  ⭐ - MIT License
  

- [sanity](https://github.com/sanity-io/sanity) - The Sanity Studio – Collaborate in real-time on structured content - 3740  ⭐ - MIT License
  

- [porter](https://github.com/porter-dev/porter) - Kubernetes powered PaaS that runs in your own cloud. - 3506  ⭐ - Other
  

- [taskcafe](https://github.com/JordanKnott/taskcafe) - An open source project management tool with Kanban boards - 3407  ⭐ - MIT License
  

- [GGEditor](https://github.com/alibaba/GGEditor) - A visual graph editor based on G6 and React - 3319  ⭐ - MIT License
  

- [BoostNote-App](https://github.com/BoostIO/BoostNote-App) - Boost Note is a document driven project management tool that maximizes remote DevOps team velocity. - 3175  ⭐ - Other
  

- [dnote](https://github.com/dnote/dnote) - A simple command line notebook for programmers - 2389  ⭐ - Other
  

- [mattermost-webapp](https://github.com/mattermost/mattermost-webapp) - Webapp of Mattermost server: https://github.com/mattermost/mattermost-server - 2121  ⭐ - Apache License 2.0
  

- [letterpad](https://github.com/letterpad/letterpad) - Letterpad is an open-source and a high performant publishing engine, built with react &amp; graphql and runs ridiculously fast 🚀 - 563  ⭐ - MIT License
  

- [lenster](https://github.com/lensterxyz/lenster) - Lenster is a decentralized, and permissionless social media app built with Lens Protocol 🌿  - 350  ⭐ - MIT License
  


